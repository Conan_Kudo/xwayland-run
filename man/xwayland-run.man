'\" t
.\"
.\" Copyright © 2023 Red Hat, Inc
.\"
.\" This program is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License as
.\" published by the Free Software Foundation; either version 2 of the
.\" License, or (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful, but
.\" WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
.\" General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
.\" 02111-1307, USA.

.ds q \N'34'
.TH XWAYLAND-RUN @appmansuffix@
.SH NAME
xwayland-run \- Run an X11 client on a dedicated \fIXwayland\fP server.
.SH SYNOPSIS
.B xwayland-run
[Xwayland options ...] -- Xclient [Xclient options ...]
.SH DESCRIPTION
.I xwayland-run
is used to spawn a given X11 client on a dedicated \fIXwayland\fP server
running rootful in a Wayland environment.
.SH "SEE ALSO"
\fIXwayland\fP(@appmansuffix@)
